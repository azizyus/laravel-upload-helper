
# What does this package do? <br>  
  
its a very basic wrapper and helper for UploadedFile class in Laravel, <br>  
it helps you while *saving* and *naming* your uploaded files. (or resizing them but you need an extra <a href="https://gitlab.com/azizyus/laravel-upload-helper-image-treatment-implementations.git"> package </a> for that)  
  
  
  
# Any Examples I Can Use?  
  
<pre>
   public function UploadControllerMethod(Request $request)  
   {  
       //"file" is you file input name  
       $file = UploadedFileCatcher::catchFile("file"); //catch your file, return value is IUploadedFile  
       $uploadHelper1 = new UploadHelper($file); //pass that file as param to UploadHelper  
       $uploadHelper1->setNamingPolicy(new RandomNamePolicy());  //an implementation which is renames your uploaded file  
       $uploadHelper1->setFileTreatment(new StandardFileTreatment()); //need to process your file?, do it here  
       $uploadHelper1->saveFile(); //save the file where ever you want, default is
   } 
</pre>  
  
  
# whats that bunker?  
  
Let's assume you uploaded an X file so you need different variants of em like a, 100x100+prefix named and original+random named, 
guess what?  

<pre>
        $file = UploadedFileCatcher::catchFile("file"); 
        $prefixed100x100UploadHelper = new UploadHelper($file); 
        $prefixed100x100UploadHelper->setNamingPolicy(new RandomPrefixedNamePolicy("my-prefix"));   
        $prefixed100x100UploadHelper->setFileTreatment(new FixedSizeImageFileTreatment(100,100)); //width, height 
        //as you can see i didnt saved it
        
        
        $originalNamedImageWithRandomName = new UploadHelper($file); 
        $originalNamedImageWithRandomName->setNamingPolicy(new NormalNamingPolicy());   
        $originalNamedImageWithRandomName->setFileTreatment(new StandardFileTreatment()); 
        
        $uploadBunker = new UploadBunker();
        $uploadBunker->addAmmunition($prefixed100x100UploadHelper)->addAmmunition($originalNamedImageWithRandomName);
        $uploadBunker->fire();
        //these will give you 2 version of that file you uploaded 
</pre>

