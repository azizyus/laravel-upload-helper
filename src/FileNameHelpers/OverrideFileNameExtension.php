<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/13/19
 * Time: 12:48 AM
 */

namespace LaravelUploadHelper\FileNameHelpers;

class OverrideFileNameExtension
{


    public static function override(String $fileName,$oldExtension,String $newExtension) : String
    {

        $newFileName = str_replace($oldExtension,$newExtension,$fileName);

        return $newFileName;

    }

}