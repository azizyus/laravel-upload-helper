<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 9:51 PM
 */

namespace LaravelUploadHelper\UploadedFileImplementations;

interface IUploadedFile
{

    public function move(string $directory, string $name = null);

    /**
     * @param $source
     * @param $target
     * @return mixed
     * @deprecated
     */
    public function copy($source,$target);
    public function getClientOriginalName();
    public function getClientOriginalExtension();
    public function getInputName();
    public function setInputName($inputName): void;
}
