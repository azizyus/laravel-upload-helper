<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 9:49 PM
 */


namespace LaravelUploadHelper\UploadedFileImplementations;


use Illuminate\Http\UploadedFile;

class UploadedFileExtended extends UploadedFile implements IUploadedFile
{

    protected $inputName=null;

    /**
     * @return null
     */
    public function getInputName()
    {
        return $this->inputName;
    }

    /**
     * @param null $inputName
     */
    public function setInputName($inputName): void
    {
        $this->inputName = $inputName;
    }


    public function copy($savePath,$fileName)
    {
        copy($this->getRealPath(),"$savePath/$fileName");
    }



}
