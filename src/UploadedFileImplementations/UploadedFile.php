<?php


namespace LaravelUploadHelper\UploadedFileImplementations;

use Illuminate\Http\UploadedFile as BaseUploadedFile;

class UploadedFile extends BaseUploadedFile implements IUploadedFile
{

    protected $inputName;

    public function copy($savePath,$fileName)
    {
        copy($this->getRealPath(),"$savePath/$fileName");
    }

    public function getInputName()
    {
        return $this->inputName;
    }

    public function setInputName($inputName): void
    {
        $this->inputName = $inputName;
    }


}
