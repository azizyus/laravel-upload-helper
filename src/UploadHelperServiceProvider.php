<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 12.01.2019
 * Time: 21:25
 */

namespace LaravelUploadHelper;

class UploadHelperServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function register()
    {

        $this->mergeConfigFrom(__DIR__."/Config/upload-helper-config.php","upload-helper-config");


        $this->publishes([

            __DIR__."/Config/upload-helper-config.php" => config_path("upload-helper-config.php")

        ],"azizyus/upload-helper-config");


    }

    public function boot()
    {

    }

}
