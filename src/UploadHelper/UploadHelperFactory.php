<?php


namespace LaravelUploadHelper\UploadHelper;


class UploadHelperFactory
{

    public function make($file) : UploadHelper
    {
        return new UploadHelper($file);
    }

}
