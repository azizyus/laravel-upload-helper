<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 7:55 PM
 */

namespace LaravelUploadHelper\UploadHelper;


use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use LaravelUploadHelper\FileTreatments\AbstractFileTreatment;
use LaravelUploadHelper\FileTreatments\StandardFileTreatment;
use LaravelUploadHelper\NamePolicies\AbstractNamePolicy;
use LaravelUploadHelper\NamePolicies\NormalNamingPolicy;
use LaravelUploadHelper\UploadedFileImplementations\IUploadedFile;


class UploadHelper
{


    protected $file;
    protected $fileName;
    /**
     * @var AbstractFileTreatment
     */
    protected $fileTreatment;
    /**
     * @var AbstractNamePolicy
     */
    protected $namingPolicy;

    /**
     * @var FilesystemAdapter
     */
    protected $disk;

    public function __construct(IUploadedFile $file)
    {
        $this->file = $file;
        $this->fileName = $file->getClientOriginalName();

        $this->setFileTreatment(new StandardFileTreatment());
        $this->setNamingPolicy(new NormalNamingPolicy());
        $this->setDisk(Storage::disk('public'));
    }

    public function setDisk(FilesystemAdapter $f)
    {
        $this->disk = $f;
    }

    public function getFileInputName()
    {
        return $this->file->getInputName();
    }

    public function setFileTreatment(AbstractFileTreatment $fileTreatment)
    {
        $this->fileTreatment = $fileTreatment;
        $this->fileTreatment->setFile($this->file);
    }
    public function setNamingPolicy(AbstractNamePolicy $namingPolicy)
    {
        $this->namingPolicy = $namingPolicy;
        $this->namingPolicy->setFileName($this->fileName);
    }

    public function getClientOriginalFileName()
    {
        return $this->fileName;
    }

    protected function getSavePath() : String
    {
        return config("upload-helper-config.uploadDir");
    }

    public function saveFile() : String
    {
        $newFileName = $this->namingPolicy->getNewFileName();
        $result = $this->fileTreatment->processUploadedFile($newFileName,$this->disk->path(''));
        $this->disk->put($newFileName,$result);
        return $newFileName;
    }




}
