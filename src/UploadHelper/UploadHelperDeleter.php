<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/20/19
 * Time: 8:58 PM
 */

namespace LaravelUploadHelper\UploadHelper;


use Illuminate\Support\Facades\File;

class UploadHelperDeleter
{

    public $uploadDir;
    public function __construct()
    {
        $this->uploadDir = config("upload-helper-config.uploadDir");
    }

    public function delete($fileName)
    {
        $filePath = "{$this->uploadDir}/$fileName";
        File::delete($filePath);

    }

}
