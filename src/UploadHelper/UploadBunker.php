<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 9:28 PM
 */

namespace LaravelUploadHelper\UploadHelper;


class UploadBunker
{


    protected  $ammunitions;
    public function __construct()
    {
        $this->ammunitions = collect();
    }

    public function addAmmunition(UploadHelper $helper,$key)
    {
        $this->ammunitions->put($key,$helper);

        return $this;
    }

    public function fire() : array
    {
        $fileNames = [];
        foreach ($this->ammunitions as $key => $pipe)
        {
            $fileNames[$key] = $pipe->saveFile();
        }

        return $fileNames;
    }

}
