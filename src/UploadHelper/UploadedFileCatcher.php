<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 9:58 PM
 */

namespace LaravelUploadHelper\UploadHelper;


use Illuminate\Http\Request;
use LaravelUploadHelper\UploadedFileImplementations\UploadedFile;
use LaravelUploadHelper\UploadedFileImplementations\UploadedFileExtended;

class UploadedFileCatcher
{

    public static $isTest = false;

    public static function fake()
    {
        static::$isTest = true;
    }

    /**
     * @param $inputName
     * @return UploadedFileExtended
     * @deprecated bad for tests, do not use it
     */
    public static function catchFile($inputName)
    {
        $file = $_FILES[$inputName];

        $instance = new UploadedFileExtended($file["tmp_name"],$file["name"],$file["type"],$file["error"],static::$isTest);
        $instance->setInputName($inputName);
        return $instance;

    }

    /**
     * @param Request $request
     * @param $inputName
     * @return UploadedFile
     */
    public static function catchFileFromRequest(Request $request,$inputName)
    {
        $file = $request->file($inputName);
        $instance = new UploadedFile($file->path(),$file->getClientOriginalName(),$file->getMimeType(),$file->getError(),static::$isTest);
        $instance->setInputName($inputName);
        return $instance;
    }

}
