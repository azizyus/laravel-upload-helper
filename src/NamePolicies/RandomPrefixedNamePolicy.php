<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 10:17 PM
 */

namespace LaravelUploadHelper\NamePolicies;


class RandomPrefixedNamePolicy extends RandomNamePolicy
{


    public $prefix;
    public function __construct(String $prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return string
     * override a method which is comes from abstract class to define new implementation of naming files
     */
    public function getNewFileName()
    {
        $randomFileName = parent::getNewFileName();
        $randomFileName = "{$this->prefix}$randomFileName";
        return $randomFileName;
    }



}