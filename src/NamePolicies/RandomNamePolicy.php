<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 7:21 PM
 */


namespace LaravelUploadHelper\NamePolicies;

class RandomNamePolicy extends AbstractNamePolicy
{

    /**
     * @return string
     * override a method which is comes from abstract class to define new implementation of naming files
     */
    public function getNewFileName()
    {
        $extension = $this->findExtension();
        $randomString = md5(uniqid());
        return $randomString.'.'.$extension;
    }

}