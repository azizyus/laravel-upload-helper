<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 7:25 PM
 */


namespace LaravelUploadHelper\NamePolicies;

abstract class AbstractNamePolicy
{

    protected $fullFileName;
    public function setFileName(String $fileFullName)
    {
        $this->fullFileName = $fileFullName;
    }

    protected function findExtension()
    {
        return pathinfo($this->fullFileName, PATHINFO_EXTENSION);
    }

    public function getNewFileName()
    {
        return $this->fullFileName;
    }

}