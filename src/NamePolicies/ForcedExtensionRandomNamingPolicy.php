<?php


namespace LaravelUploadHelper\NamePolicies;


class ForcedExtensionRandomNamingPolicy extends AbstractNamePolicy
{

    public $extension;
    public function __construct(string $extension)
    {
        $this->extension = $extension;
    }


    public function getNewFileName()
    {
        $randomString = md5(uniqid());
        return $randomString.'.'.$this->extension;
    }

}