<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 8:12 PM
 */

namespace LaravelUploadHelper\FileTreatments;


interface ISaveable
{

    /**
     * @param String $fileName
     * @param String $savePath
     * @return null|string
     */
    public function processUploadedFile(String $fileName, String $savePath);

}
