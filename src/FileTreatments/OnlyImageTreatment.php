<?php


namespace LaravelUploadHelper\FileTreatments;

class OnlyImageTreatment extends AbstractFileTreatment
{
    public function processUploadedFile(String $fileName, String $savePath)
    {
        if(in_array($this->fileNameExtension,['jpg','jpeg','png','gif','svg']))
            return $this->file->get();
        return null;
    }
}

