<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 7:18 PM
 */

namespace LaravelUploadHelper\FileTreatments;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use LaravelUploadHelper\UploadedFileImplementations\IUploadedFile;

abstract class AbstractFileTreatment implements ISaveable
{


    protected $file;
    protected $fileNameExtension;



    public function setFile(IUploadedFile $file)
    {
        $this->file = $file;
        $this->fileNameExtension = $file->getClientOriginalExtension();
    }

    public function getUploadedFile()
    {
        return $this->file;
    }

    abstract public function processUploadedFile(String $fileName, String $savePath);


}
